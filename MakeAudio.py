#from gtts import gTTS
from TTS import myTTS
import sys, os, re


output_ext = '.mp3'
output_folder = 'audios'
input_ext = '.txt'
input_folder = 'chapters'

start = int(input('input start chapter: '))
end = int(input('input end chapter: '))
    
start_ch = int(start)
end_ch = int(end) + 1
if start_ch > end_ch:
    print('start chapter must be smaller than end chapter')
    sys.exit()
if start_ch < 0 or end_ch < 0:
    print('chapter number must be positive')
    sys.exit()
    
chapter_dict = {}
    
for dirpath, dirnames, files in os.walk(input_folder):
    for file_name in files:
        parts = re.split(' - ', file_name)
        chapter_idx = int(parts[0])
        file_name = file_name.replace('.txt', '')
        chapter_dict[chapter_idx] = file_name

for index in range(start_ch, end_ch):
    output_mp3 = os.path.join(output_folder, chapter_dict[index] + output_ext)
    input_ch = os.path.join(input_folder, chapter_dict[index] + input_ext)
    
    print(input_ch, ">>", output_mp3)
    try:
        with open (input_ch, "r") as myfile:
            data = myfile.read()
            tts = myTTS(text=data,lang='zh-CN')
            tts.save(output_mp3)
    except Exception as e:
        print(e)
    print()