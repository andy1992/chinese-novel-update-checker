import re, requests, time, os, csv, shutil, sys
from Web69Source import Web69
from TTS import myTTS

class UpdateSearcher:
    
    def __init__(self):
        print("Update Searcher Initiated")
        self.updated_chapters = {}
        self.audio_novels = []
        self.today_date = time.strftime("%Y%m%d")
        punc = '\/:*?"<>|"\n'
        punc_list = [re.escape(c) for c in punc]
        self.pattern = '|'.join(punc_list)
        self.remove_regex_list = ["<[^>]+>", "（[^>]+）", "\([^>]+\)"]
        
        
    def SearchUpdate(self):
        
        if not os.path.exists("NovelList.csv"):
            print("NovelList.csv does not exist")
            sys.exit()
            
        new_info = []
        
        with open("NovelList.csv", "r") as csvfile:
        
            csv_reader = csv.reader(csvfile)
            next(csv_reader, None)
       
            for novel_info in csv_reader:

                # Creates all files necessary if it does not exist before
                if not os.path.isdir(novel_info[0]):
                    os.makedirs(novel_info[0])
                    os.makedirs(os.path.join(str(novel_info[0]), "chapters"))
                    os.makedirs(os.path.join(str(novel_info[0]), "audios"))
                    
                newest_chapter, af_status = self.Update(novel_info)
                
                novel_info[3] = str(newest_chapter)
                novel_info[4] = af_status
                
                new_info.append(novel_info)
                

                
        os.remove("NovelList.csv")
                
        with open("NovelList.csv", "w") as csvfile:
        
            csv_writer = csv.writer(csvfile, lineterminator = "\n")
            
            csv_writer.writerow(["title", "source", "novel_id", "newest_chapter_idx", "make_af"])
            
            for novel_info in new_info:
            
                csv_writer.writerow(novel_info)
                
        self.MakeAudioFiles()
            
                    
    # Parses the information about each novel and select the right source parser to use for each novel
    def Update(self, novel_info):
    
        novel_name = novel_info[0]
        web_source = novel_info[1]
        novel_id = novel_info[2]
        newest_chapter_idx = int(novel_info[3])
        af_status = novel_info[4]
        
        souce = None
        new_af_status = None
        
        if newest_chapter_idx < 0:
            print("Bad starting chapter for " + novel_name + ". newest_chapter_idx must be a positive integer.")
            sys.exit()
        
        if web_source == "web69":
            source = Web69(novel_name, novel_id)
            
        if af_status == "Y":
            if newest_chapter_idx > 0:
                self.audio_novels.append(novel_name)
            new_af_status = "Y"
            
        elif af_status == "N":
            new_af_status = "N"
            
        else:
            print("Unknown af_status flag: " + af_status + ". Only valid flags are: Y, N")
            new_af_status = "N"
        
            
        return self.GrabNewest(source, newest_chapter_idx), new_af_status
        
        
    # Used only to grab the newest chapter that has not been downloaded
    def GrabNewest(self, source, starting_chapter_idx):
        
        url_list = source.ExtractMenuURL()
        total_chapters = len(url_list)
        
        download_chapters_url = url_list[starting_chapter_idx:]
        
        novel_name = source.GetNovelTitle()
        
        print(novel_name + " released " + str(len(download_chapters_url))+" chapters")

        newest_chapter_idx = self.GetTextFromURL(source, download_chapters_url, starting_chapter_idx)
        
        return newest_chapter_idx
        
    
    # Put the text from extracted URL into the text file
    def GetTextFromURL(self, source, url_list, starting_chapter_idx):
        
        novel_name = source.GetNovelTitle()
        
        chapter_number = starting_chapter_idx
        
        whole_text_file_name = novel_name+".txt"    

        whole_file = None
        
        if starting_chapter_idx > 0:
            whole_file = open(os.path.join(novel_name, whole_text_file_name), "a")
            
        elif starting_chapter_idx == 0:
            whole_file = open(os.path.join(novel_name, whole_text_file_name), "w")
        
        new_chapter_names = []
            
        
            
        for url in url_list:
        
            chapter_number += 1
        
            chapter_name, texts = source.ExtractText(url)
            
            chapter_name = self.FileNameCleaner(chapter_name)
            
            time.sleep(0.05)
            sys.stdout.write('\r'+' '*50)
            sys.stdout.flush()
            sys.stdout.write("\rDownloading: " + chapter_name)
            
        
            file_name = str(chapter_number) + " - " + chapter_name
            
            whole_file.write("^")
            
            chapter_file_location = os.path.join(novel_name, "chapters", file_name + ".txt")
            
            with open(chapter_file_location, "w") as text_file:
            
                for line in texts:
                    text_file.write(line)
                    text_file.write("\n")
                    whole_file.write(line)
                    whole_file.write("\n")
                    
                whole_file.write("\n\n\n")
            

            if starting_chapter_idx > 0:
                self.PlaceIntoDailyRead(novel_name, chapter_file_location, file_name + ".txt")
            # shutil.copyfile(chapter_file_location, os.path.join("DailyRead", self.today_date, novel_name + "_" + file_name + ".txt"))
                
            new_chapter_names.append(file_name)
            
            
            
        print("")
        
        # if starting_chapter_idx is 0, it is first time a novel is being downloaded will not be added to daily read.
        if starting_chapter_idx > 0:
            self.updated_chapters[novel_name] = new_chapter_names
            
        whole_file.close()
        
        return chapter_number
        
        
    # Make audio file of new chapters
    def MakeAudioFiles(self):
    
        if not os.path.exists("DailyRead"):
            os.mkdir("DailyRead")
            
        daily_read_location = os.path.join("DailyRead", self.today_date)
            
        if not os.path.exists(daily_read_location):
            os.mkdir(daily_read_location)
        
        for novel in self.audio_novels:
            
            chapters = self.updated_chapters[novel]
            
            for chapter_name in chapters:
            
                input_chapter_name = os.path.join(novel, "chapters",chapter_name + ".txt")
                output_audio_dest = os.path.join(novel,"audios",chapter_name + ".mp3")
                daily_read_file_name = novel + "_" + chapter_name

                try:
                    with open (input_chapter_name, "r") as chapter_file:
                        print("Making audio file for: " + chapter_name)
                        data = chapter_file.read()
                        tts = myTTS(text=data,lang='zh-CN')
                        tts.save(output_audio_dest)
                        # shutil.copyfile(output_audio_dest, os.path.join(daily_read_location, daily_read_file_name + ".mp3"))
                        self.PlaceIntoDailyRead(novel, output_audio_dest, chapter_name + ".mp3")
                        
                            
                except:
                    print("Problem making audio file of " + novel + ":" + chapter_name)
        
        
    # Cleans up the title
    def FileNameCleaner(self, title):
    
        for remove_regex in self.remove_regex_list:
            title = re.sub(remove_regex, "", title)
        
        return re.sub(self.pattern, "", title)
        
        
    def PlaceIntoDailyRead(self, novel_name, original_file_location, file_name):
        new_file = novel_name + "_" + file_name
        shutil.copyfile(original_file_location, os.path.join("DailyRead", self.today_date, new_file))
        
        
        