import re, requests, time, os

# Used to extract novel text from http://www.69shu.com/txt/

class Web69:
    
    def __init__(self, novel_name, novel_id):
        
        self.text_page = "http://www.69shu.com/txt/"
        self.menu_page = "http://www.69shu.com/"
        self.remove_text_list = ["b'", "&nbsp;", "txttopshow7();", "~", "～", "^", "'"]
        self.remove_regex_list = ["<[^>]+>", "（[^>]+）", "\([^>]+\)"]
        self.novel_name = novel_name
        self.novel_id = int(novel_id)

            
    # Extracts the novel content from a given URL
    # Must be implemented in other source parser files
    def ExtractText(self, url):
    
        found_title = False
        
        chapter_name = None
        
        texts = []
  
        is_chapter_url = False
        
        self.Download(url)
        
        with open("temp", "r") as web_file:

            for line in web_file:

                if not found_title:
                    if line.find("chaptername") > -1:
                        found_title = True
                        chapter_name = self.ExtractTitle(line)
                        texts.append(chapter_name)
                        
                        
                # After the title is found, starts looking for the body
                        
                if found_title:
                    if line.find("&nbsp;") > -1:
                        found_start = True
                        texts.append(line)

                    
        os.remove("temp")
                    
        clean_text = self.CleanText(texts)
        
        return chapter_name, clean_text
        
    
    # Extract all the chapter URL from the menu page
    # Must be implemented in other source parser files
    def ExtractMenuURL(self):
        
        url = self.AssembleMenuURL(self.novel_id)
        
        search_url = '<li><a href="/txt/' + str(self.novel_id) + '/'
        
        self.Download(url)
        
        url_list = []
        
        with open("temp", "r") as web_file:
        
            for line in web_file:
    
                if line.find(search_url) > -1:
                    url_list.append(self.GetLineURL(line))
    
        os.remove("temp")
            
        return url_list[6:]
        
        
    # Returns the title of the novel
    # Must be implemented in other source parser files
    def GetNovelTitle(self):
        return self.novel_name
        
                
    # Cleans the HTML syntax from each line and returns the chapter contents
    def CleanText(self, lines):
    
        clean_text = []
    
        for line in lines:
            
            for remove_text in self.remove_text_list:
                line = line.replace(remove_text, "")
                
            for remove_regex in self.remove_regex_list:
                line = re.sub(remove_regex, "", line)
                
            clean_text.append(line)
            
        return clean_text
        
        
    # Downloads the HTML file of the input URL
    def Download(self, url):
        
        with open("temp", "wb") as web_file:
        
            r = requests.get(url)
            
            for chunk in r.iter_content(chunk_size = 1024):
                web_file.write(chunk)

                
    # Returns the title of the chapter
    def ExtractTitle(self, line):
        title = re.search("'(.+?)'", line).group(1)
        title = title + '\n'
        return title
        
    
    # Returns the URL for the next chapter
    def GetLineURL(self, line):
        url = re.search('<a href="(.+?)">', line).group(1)
        began_index = url.find("/", 5)
        url = url[began_index+1:]
        next_chapter_id = int(url)
        next_url = self.AssembleChapterURL(self.novel_id, next_chapter_id)
        return next_url
        
        
    # Assembles the chapter URL from book ID and chapter ID    
    def AssembleChapterURL(self, novel_id, chapter_id):
        complete_url = self.text_page + str(novel_id) + "/" + str(chapter_id)
        return complete_url
        
        
    # Assemble the menu URL from book ID
    def AssembleMenuURL(self, novel_id):
        complete_url = self.menu_page + str(novel_id) + "/"
        return complete_url
        