#Purpose

The purpose of this script is to check for updates on certain on-going Chinese web novels from a web source and extract the novel content from the HTML page. The novel contents can be made into audio format(Using google text to speech) if specified by the user.

So far, the only web novel source support at the moment is:
http://www.69shu.com/


#Installation

1. The user must have Python3.X installed and properly configured with the following packages installed: re, requests, time, os, csv, shutil, sys.

    For Window users, they must change the system locale to Chinese(Simplified, PRC). For help: http://windows.microsoft.com/en-us/windows/change-system-locale#1TC=windows-7

2. The user must properly configure "NovelList.csv".
    
    NovelList.csv is read by the script to know what novel to look out for, where to look for the novel and to keep track of the newest chapter.
    NovelList.csv contains 5 fields.
    
    title: This is the title of the novel. It is only used to know what folder to store the contents in.

    source: This tells the script which parser to use to get the contents. Only supported parser so far is: "web69".

    novel_id: This is the id assigned to the novel on that specific source. (ie: 极品富二代 is found at http://www.69shu.com/17182/. Its novel_id is 17182).

    newest_chapter_idx: This is the index of the newest chapter. This number does not necessarily match the newest chapter number because the author and websites sometimes messes up the ordering. The first time running the script, set this value to 0 to download the entire novel.

    make_af: This flag lets the script know if you want the newly downloaded chapters to be made into audio files. "Y" for yes, "N" for no. The first time a certain novel is being downloaded, the contents will NOT be made into audio files even if make_af is set to Y because creating audio file for the entire novel usually takes too long. There is another script included that takes care of that. 

3. After NovelList.csv is properly configured, run FetchUpdate.py by double clicking it or run it in cmd.exe.

    The first time the script run on a certain novel(its newest_chapter_idx is set to 0), it will not be placed in the DailyRead folder.
    After running for the first time on a novel, next time it fetches updates for it will be added to the DailyRead folder in its corresponding date location.
    
    optional: If you want to make an entire novel into audio, copy TTS.py and MakeAudio.py into the folder of the novel you want to read and run MakeAudio.py. It is suggested to make small batch(50-100 chapters) at a time. 



